/*************************************************************************
 * util.h
 *
 * Matt Shelton	<matt@mattshelton.com>
 *
 * This header file contains information relating to the bstring/util.c
 * module.
 *
 * Copyright (C) 2004 Matt Shelton <matt@mattshelton.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: util.h,v 1.2 2005/11/02 23:56:12 jfs Exp $
 *
 **************************************************************************/

#ifndef _BSTRING_UTIL_H
#define _BSTRING_UTIL_H

/* INCLUDES ---------------------------------------- */
#include "bstrlib.h"

/* PROTOTYPES -------------------------------------- */
int bltrim (bstring string);
int brtrim (bstring string);
int btrim (bstring string);

#endif
