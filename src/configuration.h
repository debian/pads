/*************************************************************************
 * configuration.h
 *
 * This module stores functions related to the configuration of the
 * pads project.
 *
 * The purpose of this system is to determine network assets by passively
 * listening to network traffic.
 *
 * Copyright (C) 2004 Matt Shelton <matt@mattshelton.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: configuration.h,v 1.3 2005/11/02 23:56:12 jfs Exp $
 *
 **************************************************************************/

#ifndef _PADS_CONFIGURATION_H
/* DEFINES ----------------------------------------- */
#define _PADS_CONFIGURATION_H	1
#ifdef LINUX
#ifndef __FAVOR_BSD
#define __FAVOR_BSD
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#endif /* ifdef LINUX */


/* INCLUDES ---------------------------------------- */
#include <stdio.h>
#include "global.h"
#include "util.h"
#include "bstring/bstrlib.h"
#include "bstring/util.h"

/* PROTOTYPES -------------------------------------- */
void init_configuration (bstring conf_file);
void parse_line (bstring line);
int conf_module_plugin (bstring value, int (*ptrFunc)(bstring, bstring));

/* External Prototypes */
int activate_output_plugin (bstring name, bstring args);
#endif
